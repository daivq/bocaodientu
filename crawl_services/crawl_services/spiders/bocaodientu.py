# -*- coding: utf-8 -*-

import os
import time
import logging
from datetime import datetime
import csv

import selenium
import textract
from selenium import webdriver
from scrapy.spiders.init import InitSpider
from selenium.webdriver.chrome.options import Options
from ..config import DOWNLOAD_DIR

logging.basicConfig(level=logging.INFO)
_logger = logging.getLogger()


def parse_content(driver, writer):
    loading_page_count = time.time()
    while True:
        if not driver.find_elements_by_class_name('gridview'):
            time.sleep(1)
            continue
        if (time.time() - loading_page_count) > 5:
            driver.quit()
            return None, None
        break

    table = driver.find_element_by_class_name('gridview')
    rows = table.find_elements_by_tag_name('tr')
    if not rows:
        return None, None

    for row in rows[1:20]:
        phone, email = '', ''
        current_files = os.listdir(DOWNLOAD_DIR)

        cols = row.find_elements_by_tag_name('td')
        if not cols:
            continue

        register_time = cols[0].text.split()[0]
        enterprise_name = cols[1].find_element_by_tag_name('p').text
        enterprise_code = cols[1].find_element_by_tag_name('span').text
        province = cols[2].text

        try:
            os.listdir('../pdf_dir')
        except Exception:
            os.mkdir('../pdf_dir')

        download_file_move_name = enterprise_code.split(': ')[1] + '.pdf'
        if download_file_move_name in os.listdir('../pdf_dir'):
            continue

        try:
            cols[4].find_element_by_tag_name('input').click()
        except Exception:
            continue

        time.sleep(3)

        check_files = os.listdir(DOWNLOAD_DIR)
        download_file_set = set(check_files) - set(current_files)
        download_file_name = download_file_set.pop()

        download_file_path = os.path.join(DOWNLOAD_DIR, download_file_name)

        download_file_move = os.path.join('../pdf_dir', download_file_move_name)
        os.rename(download_file_path, download_file_move)

        text_encoding = textract.process(download_file_move, method='tesseract', language='vie')
        texts = text_encoding.decode('utf-8').split('\n')

        is_address, address = False, ''
        for text in texts:
            if 'Ngày thành lập' in text:
                register_time = text.split(':')[1].strip()
                if ' ' in register_time:
                    register_time = register_time.split()[1]
            if 'Điện thoại:' in text and 'Fax:' in text:
                is_address = False
                try:
                    phone = text.strip().split('Điện thoại:')[1].split('Fax:')[0].strip().replace(' ', '')
                except Exception:
                    phone = ''
            elif 'Điện thoại:' in text:
                is_address = False
                try:
                    phone = text.split('Điện thoại:')[1].strip().split()[0]
                except Exception:
                    phone = ''
                if phone.endswith(":"):
                    phone = ''
            else:
                pass

            if 'Email:' in text:
                try:
                    email = text.split('Email:')[1].strip().split()[0].replace('(', '').strip()
                    if email.endswith('.co'):
                        email = email.replace('.co', '.com')
                except Exception:
                    email = ''
                if email.endswith(":"):
                    email = ''
            if is_address and text:
                address += ' ' + text
            if 'Địa chỉ trụ sở chính:' in text:
                is_address = True

        data = {
            "Ngày thành lập": register_time,
            "Tên công ty": enterprise_name,
            "Mã doanh nghiệp": enterprise_code.split(':')[1].strip(),
            "Địa chỉ": address.strip(),
            "Tỉnh thành": province,
            "Số điện thoại": phone,
            "Email": email
        }
        writer.writerow(data)

    pages = rows[-1].find_elements_by_tag_name('td')
    check_next_page, next_page = False, None
    for page in pages:
        if check_next_page:
            next_page = page
            break

        current_page = page.find_elements_by_tag_name('span')
        if current_page:
            check_next_page = True

    return driver, next_page


class BocaodientuSpider(InitSpider):
    name = "bocaodientu"
    start_urls = ["https://bocaodientu.dkkd.gov.vn/egazette/Forms/Egazette/DefaultAnnouncements.aspx"]

    _date = str(datetime.now().date())
    csv_name = 'bocaodientu-%s.csv' % _date
    lst_csv = os.listdir('.')

    is_addition = False
    if csv_name not in lst_csv:
        csv_file = open(csv_name, 'w')
    else:
        is_addition = True
        csv_file = open(csv_name, 'a')

    fieldnames = ["Ngày thành lập", "Tên công ty", "Mã doanh nghiệp", "Tỉnh thành", "Địa chỉ", "Số điện thoại", "Email"]

    writer = csv.DictWriter(csv_file, fieldnames=fieldnames, extrasaction='ignore',
                            quoting=csv.QUOTE_NONNUMERIC, dialect=csv.excel)
    if not is_addition:
        writer.writeheader()

    def __init__(self):
        self.driver = webdriver.Chrome(executable_path='/usr/bin/chromedriver')

    def parse(self, response):

        driver = self.driver
        driver.get(response.url)

        while True:
            driver, next_page = parse_content(driver, self.writer)

            if not next_page:
                break

            try:
                next_page = next_page.find_element_by_tag_name('a')
                next_page.click()
            except Exception:
                continue


        driver.quit()
