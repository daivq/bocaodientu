# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

from scrapy import Item, Field


class BocaodientuItem(Item):
    register_time = Field()
    enterprise_name = Field()
    enterprise_code = Field()
    province = Field()
    email = Field()
    phone = Field()
